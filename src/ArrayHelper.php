<?php

namespace App;

class ArrayHelper
{
    public static function iterableToArray(iterable $it): array
    {
        if (is_array($it)) {
            return $it;
        }

        $ret = [];
        array_push($ret, ...$it);

        return $ret;
    }

    public static function values(array $array)  {
        $lst = [];
        foreach( $array as $record ) {
            $lst[] = array_map(function ($item) {
                return floatval($item);
            }, array_values($record));
        }

        return array_values($lst);
    }
}