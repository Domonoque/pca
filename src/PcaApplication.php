<?php

namespace App;

use Doctrine\Common\Collections\ArrayCollection;
use League\Csv\Exception;
use League\Csv\Reader;
use League\Csv\Writer;
use Phpml\DimensionReduction\PCA;

class PcaApplication
{
    private $data;
    private const PATH_TO_DATA_FILE_NAME = 'resources/hayes-roth.data';
    private const PATH_TO_CSV = 'data.csv';

    public function main()
    {
        try {
            $this->initData();
            $pca = new PCA(null, 4);
            $reducedData = $pca->fit($this->data);
            $this->finishData($reducedData);
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    public function getRealData(): array
    {
        try {
            $csv = Reader::createFromPath(self::PATH_TO_DATA_FILE_NAME, 'r');
            $csv->setDelimiter(',');
            $csv->setHeaderOffset(0);

            $records = new ArrayCollection(
                ArrayHelper::iterableToArray($csv->getRecords())
            );

            return ArrayHelper::values($records->toArray());
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function initData()
    {
        $this->data = $this->getRealData();

        array_walk($this->data, function (&$item, $key) {
            array_shift($item);
        });
    }

    public function finishData(array $data)
    {
        try {
            $realData = $this->getRealData();
            array_walk($data, function (&$item, $key) use ($realData) {
                $item = array_merge([array_shift($realData[$key])], $item);
            });

            $csv = Writer::createFromPath(self::PATH_TO_CSV, 'w+');
            $csv->insertOne([
                'name',
                '1',
                '2',
                '3',
                '4'
            ]);

            $csv->insertAll($data);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
}